package com.banking.serviceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.banking.constaint.Constaint;
import com.banking.entity.Account;
import com.banking.entity.User;
import com.banking.exception.AccountNotFoundException;
import com.banking.exception.UserNotFoundException;
import com.banking.repository.AccountRepository;
import com.banking.repository.UserRepository;
import com.banking.service.AccountService;

@Service
public class AccountServiceImpl implements AccountService {

	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private UserRepository userRepository;

	@Override
	public Account createAccount(Account account, String userName) {

		User user = userRepository.findByName(userName);
		if (user == null) {
			throw new UserNotFoundException(Constaint.USER_NOT_FOUND, HttpStatus.NOT_FOUND);
		} else {

			account.setOwner(user);
			// Set createdAt and updatedAt timestamps
//	        account.setCreatedAt(new Date());
//	        account.setUpdatedAt(new Date());
			Account save = accountRepository.save(account);
			return save;
		}
	}

	@Override
	public List<Account> getAccountByUserName(String userName) {
		User findByName = userRepository.findByName(userName);
		List<Account> accounts = findByName.getAccounts();
		return accounts;
	}

	@Override
	public Account updateAccount(Account account, String userName, Integer accountId) {
		List<Account> accountByUserName = getAccountByUserName(userName);
		Account updatedAccount = accountByUserName.stream().filter(accounts -> accounts.getAccountId() == accountId)
				.findFirst().get();
		if (updatedAccount != null) {
			updatedAccount.setAccountType(account.getAccountType());
			updatedAccount.setUpdatedAt(account.getUpdatedAt());
			return accountRepository.save(updatedAccount);
		} else {
			throw new AccountNotFoundException("Account not found for id: " + accountId, HttpStatus.NOT_FOUND);
		}
	}

	@Override
	public void deleteAccount(String userName, Integer accountId) {

		List<Account> accountByUserName = getAccountByUserName(userName);
		Account account = accountByUserName.stream().filter(acc -> acc.getAccountId() == accountId).findFirst().get();
		accountRepository.delete(account);
	}

}

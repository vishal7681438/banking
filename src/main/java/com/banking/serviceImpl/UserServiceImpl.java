package com.banking.serviceImpl;

import java.util.List;import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.banking.constaint.Constaint;
import com.banking.dtos.UserDTO;
import com.banking.entity.User;
import com.banking.exception.DataNotFoundException;
import com.banking.exception.UserNotFoundException;
import com.banking.repository.UserRepository;
import com.banking.service.UserService;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private ModelMapper modelMapper;

	@Override
	public UserDTO saveUser(User user) {
		try {

			if (user != null) {
				User save = userRepository.save(user);
				UserDTO map = modelMapper.map(save, UserDTO.class);
				return map;
			}
		} catch (DataNotFoundException e) {
			log.debug(e);
		}
		throw new DataNotFoundException(Constaint.EMPTY_FIELD, HttpStatus.NOT_ACCEPTABLE);

	}

	@Override
	public UserDTO updateUser(User user, String name) {
		try {
			User getUser = userRepository.findByName(name);
			if (getUser != null) {
				getUser.setName(user.getName());
				getUser.setEmail(user.getEmail());
				getUser.setPassword(user.getPassword());

				saveUser(getUser);
				UserDTO userDTO = modelMapper.map(getUser, UserDTO.class);
				return userDTO;
			} else {
				User save = userRepository.save(user);
				UserDTO userDTO = modelMapper.map(save, UserDTO.class);
				return userDTO;
			}
		} catch (Exception e) {
			log.debug(e);
		}
		return null;
	}

	@Override
	public void deleteUser(String name) {
		User user = userRepository.findByName(name);
		if (user != null && user.getName().equalsIgnoreCase(name)) {
			userRepository.delete(user);
		}
	}

	@Override
	public List<UserDTO> getUsers() {
		List<User> allUser = userRepository.findAll();
		List<UserDTO> listOfUserDTOs = allUser.stream()
				.map(user -> modelMapper.map(user, UserDTO.class)).collect(Collectors.toList());
		return listOfUserDTOs;
	}


	@Override
	public UserDTO getUserByName(String name) {
		User findByName = userRepository.findByName(name);
		if (findByName != null && findByName.getName().equals(name)) {
			UserDTO userDTO = modelMapper.map(findByName, UserDTO.class);
			return userDTO;
		} else {
			throw new DataNotFoundException("User Not Found", HttpStatus.NOT_FOUND);
		}
	}

	public UserDTO loginUser(String email, String password) {

		User user = userRepository.findByEmailAndPassword(email, password);
		if (user.getPassword().equals(password) && user.getEmail().equals(email)) {
			UserDTO map = modelMapper.map(user, UserDTO.class);
			return map;
		} else {
			throw new UserNotFoundException(Constaint.USER_NOT_FOUND, HttpStatus.NOT_FOUND);
		}
	}

}

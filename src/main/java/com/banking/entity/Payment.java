package com.banking.entity;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "payment")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Payment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int paymentId; // (Primary Key)
	
	private Double amount;
	
	private Date paymentDate;
	
	private String paymentMethod;   //(e.g., credit card, debit card, online banking)
	
	private String status;  //(e.g., pending, successful, failed)
	
	private int accountId; // (Foreign Key referencing the Account entity)
	
	private Date createdAt;
}

package com.banking.exception;

import org.springframework.http.HttpStatus;


public class AccountNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String massage;
	private HttpStatus httpStatus;
	
	public AccountNotFoundException(String massage, HttpStatus httpStatus) {
		super();
		this.massage = massage;
		this.httpStatus = httpStatus;
	}
	

}

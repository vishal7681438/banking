package com.banking.exception;

import org.springframework.http.HttpStatus;

public class UserNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String massage;
	private HttpStatus httpStatus;
	

	public UserNotFoundException(String massage, HttpStatus httpStatus) {
		super();
		this.massage = massage;
		this.httpStatus = httpStatus;
	}
	

}

package com.banking.dtos;

import java.util.List;

import com.banking.entity.Account;
import lombok.Data;

@Data
public class UserDTO {
	
	private int userId;
	private String name;
	private String email;
	private String password;
    private List<Account> accounts;

}

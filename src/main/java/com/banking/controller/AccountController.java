package com.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banking.entity.Account;
import com.banking.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController {
	
	@Autowired
	private AccountService accountService;
	
	@PostMapping("/account/create")
	public ResponseEntity<Account> createAccount(@RequestBody Account account ,@RequestParam String name) {

		Account createAccount = accountService.createAccount(account, name);
		return new ResponseEntity<Account>(createAccount, HttpStatus.CREATED);
	}
	
	@GetMapping("/account/{userName}")
	public ResponseEntity<List<Account>> getAccountByName(@PathVariable("userName") String name) {
		
		List<Account> accountByUserName = accountService.getAccountByUserName(name);
		return new ResponseEntity<List<Account>>(accountByUserName, HttpStatus.OK);
	}
	
	@PutMapping("/account/{name}/{id}")
	public ResponseEntity<Account> updateAccount(@RequestBody Account account,@PathVariable String name,@PathVariable Integer id) {
		
		Account updateAccount = accountService.updateAccount(account, name, id);
		return new ResponseEntity<Account>(updateAccount, HttpStatus.valueOf(200));
	}
	
	@DeleteMapping("/account/{userName}/{accountId}")
	public ResponseEntity<String> deleteAccount(@PathVariable String userName,@PathVariable Integer accountId) {
		
		accountService.deleteAccount(userName, accountId);
		return ResponseEntity.ok("Account Deleted Succesfully") ;
	}
	
	

}

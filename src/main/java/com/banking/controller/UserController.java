package com.banking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banking.dtos.UserDTO;
import com.banking.entity.User;
import com.banking.service.UserService;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/users/register")
	public ResponseEntity<UserDTO> register(@RequestBody User user) {
		UserDTO saveUser = userService.saveUser(user);
		return new ResponseEntity<UserDTO>(saveUser, HttpStatus.CREATED);
	}

	@PostMapping("/users/login")
	public ResponseEntity<UserDTO> login(@RequestParam String email, @RequestParam String password) {
		UserDTO loginUser = userService.loginUser(email, password);
		return new ResponseEntity<UserDTO>(loginUser, HttpStatus.OK);
	}

	@GetMapping("users/{userName}")
	public ResponseEntity<UserDTO> getUserByName(@PathVariable("userName") String name) {
		UserDTO userByName = userService.getUserByName(name);
		return new ResponseEntity<UserDTO>(userByName, HttpStatus.OK);
	}

	@GetMapping("/users")
	private ResponseEntity<List<UserDTO>> listOfUsers() {
		List<UserDTO> users = userService.getUsers();
		return new ResponseEntity<List<UserDTO>>(users, HttpStatus.OK);
	}

}
package com.banking.service;

import java.util.List;

import com.banking.dtos.UserDTO;
import com.banking.entity.User;

public interface UserService {
	
	 UserDTO saveUser(User user);
	
	 UserDTO updateUser (User user,String name);
	 
	 void deleteUser(String name);
	 
	 List<UserDTO> getUsers();
	 
	 UserDTO getUserByName(String name);
	 
	 UserDTO loginUser(String email, String name);
	

}

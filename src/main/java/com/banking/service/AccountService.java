package com.banking.service;

import java.util.List;

import com.banking.entity.Account;


public interface AccountService {
	
	Account createAccount(Account account, String userName);
	
	List<Account> getAccountByUserName(String userName);
	
	Account updateAccount(Account account, String userName,Integer accountId);
	
	void deleteAccount(String userName,Integer accountId);
	
	
}
